import React, { Component } from "react";
import { Button, Divider, Modal, Card, Table, Form, Input } from "antd";

const FormItem = Form.Item;
const confirm = Modal.confirm;
const data = [{
  key: '1',
  name: 'John Brown',
  age: 32,
  mobile: 987654321,
  email: 'praba@gmail.com',
  address: 'New York No. 1 Lake Park',
  category: 'lawyer',
  subcategory: 'civil lawyer',
  professionalname: 'test',
}, {
  key: '2',
  name: 'Jim Green',
  age: 42,
  mobile: 6987451230,
  email: 'praba@gmail.com',
  address: 'London No. 1 Lake Park',
  category: 'lawyer',
  subcategory: 'civil lawyer',
  professionalname: 'test',
}, {
  key: '3',
  name: 'Joe Black',
  age: 32,
  mobile: 6987451230,
  email: 'praba@gmail.com',
  address: 'Sidney No. 1 Lake Park',
  category: 'lawyer',
  subcategory: 'civil lawyer',
  professionalname: 'test',
}, {
  key: '4',
  name: 'Jim Red',
  age: 32,
  mobile: 6987451230,
  email: 'praba@gmail.com',
  address: 'London No. 2 Lake Park',
  category: 'lawyer',
  subcategory: 'civil lawyer',
  professionalname: 'test',
}];

class ResetFilter extends React.Component {
  state = {
    filteredInfo: null,
    sortedInfo: null,
    visible: false
  };

  showDeleteConfirm = () => {
    confirm({
      title: 'Are you sure delete this task?',
      content: 'Some descriptions',
      okText: 'Yes',
      okType: 'danger',
      cancelText: 'No',
      onOk() {
        console.log('OK');
      },
      onCancel() {
        console.log('Cancel');
      },
    });
  }
  showModal = () => {
    this.setState({
      visible: true,
    });
  };
  handleOk = (e) => {
    console.log(e);
    this.setState({
      visible: false,
    });
  };
  handleCancel = (e) => {
    console.log(e);
    this.setState({
      visible: false,
    });
  };
  handleChange = (pagination, filters, sorter) => {
    console.log('Various parameters', pagination, filters, sorter);
    this.setState({
      filteredInfo: filters,
      sortedInfo: sorter,
    });
  };
  clearFilters = () => {
    this.setState({ filteredInfo: null });
  };
  clearAll = () => {
    this.setState({
      filteredInfo: null,
      sortedInfo: null,
    });
  };
  setAgeSort = () => {
    this.setState({
      sortedInfo: {
        order: 'descend',
        columnKey: 'age',
      },
    });
  };
  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);
      }
    });
  }
  render() {
    // const {getFieldDecorator} = this.props.form;
    let { sortedInfo, filteredInfo } = this.state;
    sortedInfo = sortedInfo || {};
    filteredInfo = filteredInfo || {};
    const columns = [{
      title: 'Name',
      dataIndex: 'name',
      key: 'name',
      filters: [
        { text: 'Joe', value: 'Joe' },
        { text: 'Jim', value: 'Jim' },
      ],
      filteredValue: filteredInfo.name || null,
      onFilter: (value, record) => record.name.includes(value),
      sorter: (a, b) => a.name.length - b.name.length,
      sortOrder: sortedInfo.columnKey === 'name' && sortedInfo.order,
    }, {
      title: 'Age',
      dataIndex: 'age',
      key: 'age',
      sorter: (a, b) => a.age - b.age,
      sortOrder: sortedInfo.columnKey === 'age' && sortedInfo.order,
    },
    {
      title: 'Mobile',
      dataIndex: 'mobile',
      key: 'mobile',
      sorter: (a, b) => a.mobile - b.mobile,
      sortOrder: sortedInfo.columnKey === 'mobile' && sortedInfo.order,
    },
    {
      title: 'Email',
      dataIndex: 'email',
      key: 'email',
    },
    {
      title: 'Category',
      dataIndex: 'category',
      key: 'category',
    },
    {
      title: 'Sub-Category',
      dataIndex: 'subcategory',
      key: 'subcategory',
    },
    {
      title: 'Professional Name',
      dataIndex: 'professionalname',
      key: 'professionalname',
    },  {
      title: 'Action',
      key: '',
      render: () =>
        <span>
          <span style={{ cursor: 'pointer' }} onClick={this.showModal} className="icon icon-edit"></span>
          <Divider type="vertical" />
          <span style={{ cursor: 'pointer' }} onClick={this.showDeleteConfirm} className="icon icon-close-circle"></span>
        </span>
    }];

    return (
      <div>
        <Card title="User Master">
          <div className="table-operations">
            <Button onClick={this.setAgeSort}>Sort age</Button>
            <Button onClick={this.clearFilters}>Clear filters</Button>
            <Button onClick={this.clearAll}>Clear filters and sorters</Button>
           
          </div>
          <Table className="gx-table-responsive" columns={columns} dataSource={data} onChange={this.handleChange} />
        </Card>
        <Modal
          title="Basic Modal"
          visible={this.state.visible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
        >
          <Form onSubmit={this.handleSubmit}>
            <FormItem
              label="UserName"
              labelCol={{ xs: 24, sm: 8 }}
              wrapperCol={{ xs: 24, sm: 16 }}
            >
              <Input />
            </FormItem>
            <FormItem
              label="Mobile"
              labelCol={{ xs: 24, sm: 8 }}
              wrapperCol={{ xs: 24, sm: 16 }}
            >
              <Input />
            </FormItem>
            <FormItem
              label="Email"
              labelCol={{ xs: 24, sm: 8 }}
              wrapperCol={{ xs: 24, sm: 16 }}
            >
              <Input />
            </FormItem>
            <FormItem
              label="Category"
              labelCol={{ xs: 24, sm: 8 }}
              wrapperCol={{ xs: 24, sm: 16 }}
            >
              <Input />
            </FormItem>
            <FormItem
              label="Sub-Category"
              labelCol={{ xs: 24, sm: 8 }}
              wrapperCol={{ xs: 24, sm: 16 }}
            >
              <Input />
            </FormItem>
            <FormItem
              label="Professional Name"
              labelCol={{ xs: 24, sm: 8 }}
              wrapperCol={{ xs: 24, sm: 16 }}
            >
              <Input />
            </FormItem>
            <FormItem
              label="Address"
               labelCol={{ xs: 24, sm: 8 }}
              wrapperCol={{ xs: 24, sm: 16 }}
            >
              <Input />
            </FormItem>
            {/* <FormItem
              wrapperCol={{ xs: 24, sm: { span: 12, offset: 5 } }}
            >
              <Button type="primary" htmlType="submit">
                Submit
            </Button>
            </FormItem> */}
          </Form>
        </Modal>
      </div>
    );
  }
}

export default ResetFilter;
